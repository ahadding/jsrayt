define(function () {
	return {
		//beget (o) returns a copy of the object o
		beget: function (o) {
			var F = function () {};
			F.prototype = o;
			return new F();
		},
		
		//solves a quadratic equation with coeffs A, B, C if possible.
		//returns false if no solutions.
		//returns true and stores solutions as sols.t0, sols.t1 otherwise
		quadratic: function (A, B, C, sols){
		
			//compute discriminant
			var discrim = B * B - 4 * A * C;
			var rootDiscrim = Math.sqrt(discrim);
			var q, t0, t1, temp;
			
			//real solution does not exist
			if (discrim <= 0){
				return false;
			}
			
			//compute quadric t values
			if (B < 0){
				q = -0.5 * (B - rootDiscrim);
			}
			else {
				q = -0.5 * (B + rootDiscrim);
			}
			
			t0 = q / A;
			t1 = C / q;
			if (t0 > t1){
				temp = t0;
				t0 = t1;
				t1 = temp;
			}
			
			sols.t0 = t0;
			sols.t1 = t1;
			
			//solution exists
			return true;
		}
	};
});