require(['Ray', 'Util', 'Vector3f', 'RayTest', 'Renderer'], function(Ray, Util, Vector3f, RayTest, Renderer) {
    //This function is called when all scripts have been loaded.
	var r = Util.beget(Ray);
	RayTest.intersectScene_threeObjectsNoneIntersect_isectHitSomethingFalse();
	RayTest.intersectScene_threeObjectsOneIntersect_isectHitSomethingTrue();
	RayTest.intersectScene_threeObjectsTwoIntersect_isectHitSomethingTrue();
	RayTest.incomingRadiance_noHit_returnBlack();
	RayTest.incomingRadiance_oneHit_returnColor();
	RayTest.incomingRadiance_oneHitOneBounce_returnColor();
	//Renderer.render();
});