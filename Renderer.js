define(['Util', 'Vector3f', 'Shape', 'Sphere', 'Ray', 'Light', 'Material'],
	function (Util, Vector3f, Shape, Sphere, Ray, Light, Material) {
	return {
		render: function () {
			//setup scene
			var scene = {
				primitives: null,
				lights: null
			};
			
			var testPrim1 = {
				shape: Util.beget(Sphere),
				material: Util.beget(Material)
			};

			testPrim1.material.diffuse = 1;
			testPrim1.material.specular = 1;
			testPrim1.material.color = Vector3f.create(1, 1, 1);
			
			var testPrim2 = Util.beget(testPrim1);
			testPrim2.material.color = Vector3f.create(110, 110, 110);
			
			var testLi1 = Light.create([0, 1, 8], [1, 1, 1], 1000);
			testLi1.scene = scene;
			
			var testLi2 = Util.beget(testLi1);
			testLi2.color = Vector3f.create(0, 1, 0);
			
			scene.primitives = [testPrim1];
			scene.lights = [testLi1];
			
			//setup a "camera" point
			var camPoint = Vector3f.create(0, 0, 0);
			var camDir = Vector3f.createUnitVector(0, 0, 1);
			alert("camDir.y: " + camDir.y);
			Ray.scene = scene;
			var camRay = Util.beget(Ray);
			camRay.o = camPoint;
			camRay.d = camDir;
			
			var color = camRay.incomingRadiance();
			alert("Radiance along ray: " + color.x + " " + color.y + " " + color.z);
		}
	};
});
