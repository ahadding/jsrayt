define(['Util', 'Vector3f'], function (Util, Vector3f) {
	
	return {
		o: null,
		d: null,
		depth: 0,
		scene: null,
		mint: 0.03,
		maxt: 99999 * 1000000,
		
		intersection: {
			current_t: 9 * 1000000,
			t: 8 * 1000000,
			hitSomething: false,
			ray: null
		},
		
		//takes an intersection object to store information regarding an intersection
		intersectScene: function (isect) {
			var i;
			isect.hitSomething = false;
			isectRay = this.getOffsetRay();
			alert("isect.t: " + isect.t);
			alert("isect.current_t " + isect.current_t);

			//go through all the objects in this.scene, calling intersect()
			//on each one and saving the information from the closest object
			for (i = 0; i < this.scene.primitives.length; i++) {
				if (this.scene.primitives[i].shape.intersect(isectRay, isect)) {
					if (isect.current_t < isect.t) {
						isect.t = isect.current_t;
						isect.primitive = this.scene.primitives[i];
						isect.point = this.o.add(this.d.mul(isect.t));
						isect.normal = this.scene.primitives[i].shape.getNormal(isect.point);
						isect.hitSomething = true;
						alert("Intersection distance returned: " + isect.t);
					}
				}
			}
		},

		//returns a copy of this ray with its origin moved forward by mint*d,
		//to be used in intersection, shading, lighting calculations
		//for the purpose of avoiding self-intersections
		getOffsetRay: function () {
			var offsetRay = Util.beget(this);
			offsetRay.o = this.o.mul(10000).add(this.d.mul(10000*this.mint)).div(10000);
			return offsetRay;
		},
		
		//returns the radiance coming along this ray from the direction it points
		incomingRadiance: function () {
			var incomingRad = Vector3f.create(0, 0, 0);
			var isect = Util.beget(this.intersection);
			var incRadAtIsectPoint = Vector3f.create(0, 0, 0);
			var specularAtIsectPoint;
			var reflectedRay;
			var i;

			//check for intersections in the scene
			this.intersectScene(isect);
			
			if (!isect.hitSomething) {
				//if there were no intersections, we're done
				return incomingRad;
			}
			else {
				//calculate and add diffuse reflection component
				alert("Coords of intersection: " + isect.point.x + ", " + isect.point.y + ", " + isect.point.z);
				for (i = 0; i < this.scene.lights.length; i++) {
					//get radiance from lights visible from our intersection point
					incRadAtIsectPoint = incRadAtIsectPoint.add(this.scene.lights[i].radiance(isect.point));
					var dif = incRadAtIsectPoint;
					alert("incRadAtIsectPoint x, y, z: " + dif.x + ", " + dif.y + ", " + dif.z);
				}
				incomingRad = incomingRad.add(incRadAtIsectPoint.
											mulComp(isect.primitive.material.color).
											mul(isect.primitive.material.diffuse));
											
				dif = incRadAtIsectPoint;
				alert("color: " + isect.primitive.material.color.x);
				alert("incRadAtIsectPoint x, y, z: " + dif.x + ", " + dif.y + ", " + dif.z);
				//calculate and add specular reflection component
				if (this.depth > 0){
					reflectedRay = Util.beget(this);
					reflectedRay.o = isect.point;
					reflectedRay.d = this.d.reflect(isect.normal).normalize();
					reflectedRay.depth--;
					specularAtIsectPoint = reflectedRay.incomingRadiance();
					incomingRad = incomingRad.add(specularAtIsectPoint.
												mulComp(isect.primitive.material.color).
												mul(isect.primitive.material.specular));
				}
				
				return incomingRad;
			}
		}
	};
});