//depends on Vector3f, Ray, Util
define(['Util', 'Ray', 'Vector3f'], function (Util, Ray, Vector3f) {
	return {
		shapeStub: {
			intersected: false,
			distance: 0,

			intersect: function (ray, isectStub) {
				isectStub.current_t = this.distance;
				return this.intersected;
			},

			getNormal: function () {
				return Vector3f.create(1, 0, 0);
			}
		},
		
		materialStub: {
			diffuse: 1,
			specular: 1,
			color: null
		},
		
		primitiveStub: {
			shape: null,
			material: null
		},

		intersectionStub: {
			current_t: 9 * 1000000,
			t: 8 * 1000000,
			hitSomething: false,
			ray: null
		},
		
		lightStub: {
			visible: false,
			distance: 0,
			intensity: { r: 100, g: 100, b: 100 },
			
			radiance: function (point) {
				if (!this.visible) {
					return Vector3f.create(0, 0, 0);
				}
				else {
					return Vector3f.create(100, 100, 100);
				}
			}
		},

		/**
			Tested method: Ray.intersectScene()
			Conditions: Three shape stubs, all with intersected = false and distance = 0
			Expected Results: isect.hitSomething === false
		*/
		intersectScene_threeObjectsNoneIntersect_isectHitSomethingFalse: function () {
			//setup
			var ray = Util.beget(Ray);
			var scene = {};
			scene.primitives = [ Util.beget(this.primitiveStub), Util.beget(this.primitiveStub),
						Util.beget(this.primitiveStub) ];
			var isect = Util.beget(this.intersectionStub);
			var i;
			
			for (i = 0; i < scene.primitives.length; i++){
				scene.primitives[i].shape = Util.beget(this.shapeStub);
			}
			
			ray.o = Vector3f.create(0, 0, 0);
			ray.d = Vector3f.create(0, 0, 1);
			ray.scene = scene;

			ray.intersectScene(isect);

			if (isect.hitSomething) {
				alert("FAIL: hit something at distance " + isect.t);
			}
			else {
				alert("PASS: hit nothing");
			}
		},
		
		/**
			Tested method: Ray.intersectScene()
			Conditions: Three shape stubs, one with intersected = true and distance = 8
			Expected Results: isect.hitSomething === true and isect.t === 8
		*/
		intersectScene_threeObjectsOneIntersect_isectHitSomethingTrue: function () {
			//setup
			var ray = Util.beget(Ray);
			var scene = {};
			scene.primitives = [ Util.beget(this.primitiveStub), Util.beget(this.primitiveStub),
						Util.beget(this.primitiveStub) ];
			var isect = Util.beget(this.intersectionStub);
			var i;
			
			for (i = 0; i < scene.primitives.length; i++){
				scene.primitives[i].shape = Util.beget(this.shapeStub);
			}
			scene.primitives[1].shape.intersected = true;
			scene.primitives[1].shape.distance = 8;

			ray.o = Vector3f.create(0, 0, 0);
			ray.d = Vector3f.create(0, 0, 1);
			ray.scene = scene;
			ray.intersectScene(isect);
			if (isect.hitSomething && isect.t === 8) {
				alert("PASS: hit something at distance " + isect.t);
			}
			else {
				alert("FAIL: hit nothing " + isect.t);
			}
		},
		
		/**
			Tested method: Ray.intersectScene()
			Conditions: Three shape stubs, two with intersected = true and distance = 8, 4
			Expected Results: isect.hitSomething === true and isect.t === 4
		*/
		intersectScene_threeObjectsTwoIntersect_isectHitSomethingTrue: function () {
			//setup
			var ray = Util.beget(Ray);
			var scene = {};
			scene.primitives = [ Util.beget(this.primitiveStub), Util.beget(this.primitiveStub),
						Util.beget(this.primitiveStub) ];
			var isect = Util.beget(this.intersectionStub);
			var i;
			
			for (i = 0; i < scene.primitives.length; i++){
				scene.primitives[i].shape = Util.beget(this.shapeStub);
			}
			scene.primitives[1].shape.intersected = true;
			scene.primitives[2].shape.intersected = true;
			scene.primitives[1].shape.distance = 8;
			scene.primitives[2].shape.distance = 4;

			ray.o = Vector3f.create(0, 0, 0);
			ray.d = Vector3f.create(0, 0, 1);
			ray.scene = scene;
			ray.intersectScene(isect);
			if (isect.hitSomething && isect.t === 4) {
				alert("PASS: hit something at distance " + isect.t);
			}
			else {
				alert("FAIL: hit nothing " + isect.t);
			}
		},
		
		/**
			Tested method: Ray.incomingRadiance()
			Conditions: three primitives, three lights. all lights visible, no primitives intersected.
			Expected Results: incomingRadiance returns a Vector3f v s.t. v.x = v.y = v.z = 0
		*/
		incomingRadiance_noHit_returnBlack: function () {
			var ray = Util.beget(Ray);
			ray.o = Vector3f.create(0, 0, 0);
			ray.d = Vector3f.createUnitVector(0, 0, 1);
			
			var primStub = Util.beget(this.primitiveStub);
			primStub.shape = Util.beget(this.shapeStub);
			primStub.material = Util.beget(this.materialStub);
			primStub.material.color = Vector3f.create(1, 1, 1);
			
			var liStub = Util.beget(this.lightStub);
			liStub.visible = true;
			
			var scene = {
				primitives: [Util.beget(primStub), Util.beget(primStub), Util.beget(primStub)],
				lights: [Util.beget(liStub), Util.beget(liStub), Util.beget(liStub)]
			};

			ray.scene = scene;
			var rad = ray.incomingRadiance();
			
			if (rad.x === 0 && rad.y === 0 && rad.z === 0) {
				alert("PASS: rad.x, y, z = " + rad.x + ", " + rad.y + ", " + rad.z);
			}
			else {
				alert("FAIL: rad.x, y, z = " + rad.x + ", " + rad.y + ", " + rad.z);
			}
		},
		
		/**
			Tested method: Ray.incomingRadiance()
			Conditions: three primitives, three lights. all lights visible, one intersecte, ray.depth = 0
			Expected Results: incomingRadiance returns a Vector3f v s.t. v.x,v.y,v.z = 300, 300, 300
		*/
		incomingRadiance_oneHit_returnColor: function () {
			var primStub = Util.beget(this.primitiveStub);
			primStub.shape = Util.beget(this.shapeStub);
			primStub.material = Util.beget(this.materialStub);
			primStub.material.color = Vector3f.create(1, 1, 1);
			
			var liStub = Util.beget(this.lightStub);
			liStub.visible = true;
			
			var scene = {
				primitives: [Util.beget(primStub), Util.beget(primStub), Util.beget(primStub)],
				lights: [Util.beget(liStub), Util.beget(liStub), Util.beget(liStub)]
			};
			scene.primitives[0].shape.intersected = true;
			scene.primitives[0].distance = 10;
			
			var ray = Util.beget(Ray);
			ray.o = Vector3f.create(0, 0, 0);
			ray.d = Vector3f.createUnitVector(0, 0, 1);
			ray.depth = 0;
			ray.scene = scene;

			var rad = ray.incomingRadiance();
			
			if (rad.x === 300 && rad.y === 300 && rad.z === 300) {
				alert("PASS: rad.x, y, z = " + rad.x + ", " + rad.y + ", " + rad.z);
			}
			else {
				alert("FAIL: rad.x, y, z = " + rad.x + ", " + rad.y + ", " + rad.z);
			}
		},
		
		/**
			Tested method: Ray.incomingRadiance()
			Conditions: three primitives, three lights. all lights visible, one intersecte, ray.depth = 1
			Expected Results: incomingRadiance returns a Vector3f v s.t. v.x,v.y,v.z = 600, 600, 600
		*/
		incomingRadiance_oneHitOneBounce_returnColor: function () {
			var primStub = Util.beget(this.primitiveStub);
			primStub.shape = Util.beget(this.shapeStub);
			primStub.material = Util.beget(this.materialStub);
			primStub.material.color = Vector3f.create(1, 1, 1);
			
			var liStub = Util.beget(this.lightStub);
			liStub.visible = true;
			
			var scene = {
				primitives: [Util.beget(primStub), Util.beget(primStub), Util.beget(primStub)],
				lights: [Util.beget(liStub), Util.beget(liStub), Util.beget(liStub)]
			};
			scene.primitives[0].shape.intersected = true;
			scene.primitives[0].distance = 10;
			
			var ray = Util.beget(Ray);
			ray.o = Vector3f.create(0, 0, 0);
			ray.d = Vector3f.createUnitVector(0, 0, 1);
			ray.depth = 1;
			ray.scene = scene;

			var rad = ray.incomingRadiance();
			
			if (rad.x === 600 && rad.y === 600 && rad.z === 600) {
				alert("PASS: rad.x, y, z = " + rad.x + ", " + rad.y + ", " + rad.z);
			}
			else {
				alert("FAIL: rad.x, y, z = " + rad.x + ", " + rad.y + ", " + rad.z);
			}
		}
	};
});