define(['Util'], function (Util) {
	return {
		r: 0,
		g: 0,
		b: 0,
		
		create: function(r, g, b){
			var v = Util.beget(this);
			v.r = r;
			v.g = g;
			v.b = b;
			return v;
		},
		
		add: function (v) {
			var w = Util.beget(this);
			w.r = this.r + v.r;
			w.g = this.g + v.g;
			w.b = this.b + v.b;
			return w;
		},
		
		sub: function (v) {
			var w = Util.beget(this);
			w.r = this.r - v.r;
			w.g = this.g - v.g;
			w.b = this.b - v.b;
			return w;
		},
		
		mul: function (k) {
			var w = Util.beget(this);
			w.r = this.r * k;
			w.g = this.g * k;
			w.b = this.b * k;
			return w;
		},
		
		div: function (k) {
			var w = Util.beget(this);
			var k_inv = 1/k;
			w.r = this.r * k_inv;
			w.g = this.g * k_inv;
			w.b = this.b * k_inv;
			return w;
		},
		
		mulComp: function (v) {
			var w = Util.beget(this);
			w.r = this.r * v.r;
			w.g = this.g * v.g;
			w.b = this.b * v.b;
			return w;
		},
		
		divComp: function (v) {
			var w = Util.beget(this);
			w.r = this.r / v.r;
			w.g = this.g / v.g;
			w.b = this.b / v.b;
			return w;
		}
	};
});