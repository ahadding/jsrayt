define(['Util'], function (Util) {
	return {
		x: 0,
		y: 0,
		z: 0,
		
		create: function(x, y, z){
			var v = Util.beget(this);
			v.x = x;
			v.y = y;
			v.z = z;
			return v;
		},
		
		createUnitVector: function(x, y, z){
			return this.create(x,y,z).normalize();
		},
		
		add: function (v) {
			var w = Util.beget(this);
			w.x = this.x + v.x;
			w.y = this.y + v.y;
			w.z = this.z + v.z;
			return w;
		},
		
		sub: function (v) {
			var w = Util.beget(this);
			w.x = this.x - v.x;
			w.y = this.y - v.y;
			w.z = this.z - v.z;
			return w;
		},
		
		mul: function (k) {
			var w = Util.beget(this);
			w.x = this.x * k;
			w.y = this.y * k;
			w.z = this.z * k;
			return w;
		},
		
		div: function (k) {
			var w = Util.beget(this);
			var k_inv = 1/k;
			w.x = this.x * k_inv;
			w.y = this.y * k_inv;
			w.z = this.z * k_inv;
			return w;
		},
		
		mulComp: function (v) {
			var w = Util.beget(this);
			w.x = this.x * v.x;
			w.y = this.y * v.y;
			w.z = this.z * v.z;
			return w;
		},
		
		divComp: function (v) {
			var w = Util.beget(this);
			w.x = this.x / v.x;
			w.y = this.y / v.y;
			w.z = this.z / v.z;
			return w;
		},
		
		dot: function (v) {
			return this.x * v.x + this.y * v.y + this.z * v.z;
		},
		
		cross: function (v) {
			var w = Util.beget(this);
			w.x = this.y * v.z - this.z * v.y;
			w.y = -this.x * v.z + this.z * v.x;
			w.z = this.x * v.y - this.y * v.x;
			return w;
		},
		
		reflect: function (normal) {
			return normal.mul(this.dot(normal) * 2).sub(this);
		},
		
		length: function () {
			return Math.sqrt( this.x * this.x + this.y * this.y + this.z * this.z );
		},
		
		length_2: function () {
			return (this.x * this.x + this.y * this.y + this.z * this.z);
		},
		
		normalize: function () {
			return this.div(this.length());
		},
		
		//returns the distance between this Vector3f and another Vector3f v,
		//assuming both are meant to represent points.
		distance: function (v) {
			return v.sub(this).length();
		}
	};
});