define(['Util'], function (Util) {
	return {
		color: null,
		diffuse: null,
		specular: null
	};
});
