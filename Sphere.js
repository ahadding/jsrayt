define(['Util', 'Vector3f', 'Shape'], function (Util, Vector3f, Shape) {

	var sphere = Util.beget(Shape);
	sphere.center = Vector3f.create(0, 0, 10);
	sphere.radius = 1;
	
	/**
	Checks whether this sphere intersects with a ray.
	Returns information regarding intersections through isect.
	isect.hitSomething = whether this object is intersected by ray.
	isect.current_t = distance along isect.ray that intersection takes place.
	*/
	//the following code assumes that the ray has been transformed into object space
	sphere.intersect = function (ray, isect) {
		var v = ray.o.sub(this.center);
		var b = (v.dot(ray.d)) * -1;
		var disc = (b * b) - (v.dot(v)) + this.radius*this.radius;

		var MISS = false;
		var HIT = true;
		var IN_HIT = true;
		var retval = MISS;
		var i1, i2, distance = 9999999999;

		if (disc > 0) {
			disc = Math.sqrt(disc);

			i1 = b - disc;
			i2 = b + disc;

			if (i2 > 0) {
				if (i1 < 0) {
					if (i2 < distance) {
						distance = i2;
						retval = IN_HIT;
					}
				}
				else {
					if (i1 < distance) {
						distance = i1;
						retval = HIT;
					}
				}
			}
		}
		
		isect.current_t = distance;
		alert("i1, i2: " + i1 + ", " + i2);
		alert("isect.current_t: " + isect.current_t);
		return retval;
	};
	
	//returns the normal on this sphere's surface at point, which is assumed
	//to be on this sphere's surface.
	sphere.getNormal = function (point) {
		return point.sub(this.center).div(this.radius);
	};
	
	return sphere;
});