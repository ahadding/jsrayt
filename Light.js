define(['Util', 'Vector3f', 'Ray'], function (Util, Vector3f, Ray) {
	return {
		position: null,
		scene: null,
		intensity: null,
		color: null,
		
		/**
		
		Creates and returns new Light object, given two arrays of numbers.
		
		*/
		create: function (position, color, intensity) {
			var li = Util.beget(this);
			li.position = Vector3f.create(position[0], position[1], position[2]);
			li.color = Vector3f.create(color[0], color[1], color[2]);
			li.intensity = intensity;
			return li;
		},
		
		/**
		Checks whether the given point's view of this light is occluded by some
		object. Returns true if so, false otherwise.
		*/
		isOccluded: function (point) {
			//create a ray from point to this light, then check whether
			//ray.intersectScene(isect) results in isect.hitSomething == true.
			//if it does, then check whether the primitive it intersected is
			//located between point and this light.
			var shadowRay = Util.beget(Ray);
			var isect = Util.beget(Ray.intersection);

			shadowRay.o = Util.beget(point);
			shadowRay.d = this.position.sub(point).normalize();

			shadowRay = shadowRay.getOffsetRay();
			shadowRay.intersectScene(isect);
			
			if (!isect.hitSomething) {
				return false;
			}
			else if (shadowRay.d.length() < isect.t){
				return false;
			}
			else {
				return true;
			}
		},
		
		/**
		Calculates and returns the amount of radiance arriving at an unoccluded
		point from this light.
		*/
		radiance: function (point) {
			//if the point and this light are unoccluded, then use an inverse-
			//square calculation to determine the amount of radiance received
			//at point
			var distanceSquared;
			if (!this.isOccluded(point)){
				distanceSquared = this.position.sub(point).length_2();
				var toLi = this.position.sub(point);
				alert("toLi x, y, z: " + toLi.x + ", " + toLi.y + ", " + toLi.z);
				alert("distance_2 from light: " + distanceSquared);
				return this.color.mul(this.intensity / distanceSquared);
			}
			else {
				return Vector3f.create(0, 0, 0);
			}
		}
	};
});